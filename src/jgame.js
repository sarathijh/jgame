var jGame = {
  // Seconds passed since the start of the game.
  time: 0,
  // Seconds passed since the previous frame.
  deltaTime: 0,
  
  // Functions to be called after jGame.Init() is called.
  _onInit: [],
  _initialized: false,
  
  // Objects that need to be rendered.
  _renderedObjects: new Set(),
  // Objects that aren't rendered, but still need an Update call.
  _nonRenderedObjects: new Set(),
  
  OnInit(cb) { jGame._onInit.push(cb); },
  
  Init(canvas, width, height, bgcolor=Color.white) {
    if (jGame._initialized) {
      console.log('The jGame library is already initialized.');
      return true;
    }
    
    if (typeof canvas === 'string') {
      canvas = document.querySelector(canvas);
    }
    if (!(canvas instanceof HTMLCanvasElement)) {
      console.log('The `canvas` parameter must be either an HTMLCanvasElement '+
                  'or a selector for an HTMLCanvasElement.');
      jGame._initialized = false;
      return false;
    }
    
    let contextParams = { alpha: false, premultipliedAlpha: false, };
    jGame.gl = canvas.getContext('webgl2', contextParams);
    if (!jGame.gl) { jGame.gl = canvas.getContext('webgl', contextParams); }
    if (!jGame.gl) {
      console.log('Unable to initialize WebGL context. Your browser may not support it.');
      jGame._initialized = false;
      return false;
    }
    
    jGame.gl.clearColor(...bgcolor);
    jGame.gl.enable(jGame.gl.BLEND);
    jGame.gl.blendFunc(jGame.gl.SRC_ALPHA, jGame.gl.ONE_MINUS_SRC_ALPHA);
    
    Object.defineProperty(canvas, 'size', { get: function() { return new Vector2(this.width, this.height); } });
    Object.defineProperty(canvas, 'center', { get: function() { return new Vector2(this.width/2, this.height/2); } });
    canvas.setSize = function(w, h) {
      this.width = w;
      this.height = h;
      jGame.projMatrix = mat4.ortho(w, h);
      jGame.gl.viewport(0, 0, w, h);
    };
    
    jGame.canvas = canvas;
    jGame.canvas.setSize(width, height);
    
    for (var cb of jGame._onInit){ cb(); }
    jGame._startGameLoop();
    
    jGame._initialized = true;
    return true;
  },
  
  _update() {
    for (let object of jGame._renderedObjects) { object.Update(); }
    for (let object of jGame._nonRenderedObjects) { object.Update(); }
  },

  _lateUpdate() {
    for (let object of jGame._renderedObjects) { object.LateUpdate(); }
    for (let object of jGame._nonRenderedObjects) { object.LateUpdate(); }
  },

  _render() {
    jGame.gl.clear(jGame.gl.COLOR_BUFFER_BIT);
    for (let object of jGame._renderedObjects) { object.Render(jGame.projMatrix); }
  },
  
  _startGameLoop() {
    let targetFPS = 120;
    
    var onFrameUpdate;
    var _requestAnimationFrame = 
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame;
    
    if (_requestAnimationFrame) {
      onFrameUpdate = function(cb) {
        var _cb = function() {
          cb();
          _requestAnimationFrame(_cb);
        }
        _cb();
      };
    } else {
      onFrameUpdate = function(cb) {
        setInterval(cb, 1000 / targetFPS);
      }
    }
    
    var isFirstFrame = true;
    var startTime = Date.now() / 1000;
    var previousTime = 0;
    
    onFrameUpdate(function() {
      jGame.time = (Date.now() / 1000) - startTime;
      if (isFirstFrame) {
        isFirstFrame = false;
        jGame.deltaTime = 1 / targetFPS;
      } else {
        jGame.deltaTime = jGame.time - previousTime;
      }
      
      Input._onBeforeFrame();
      
      if (jGame.deltaTime >= 1 / targetFPS) {
        previousTime = jGame.time;
        jGame._update();
        jGame._lateUpdate();
        jGame._render();
        Input._onAfterFrame();
      }
    });
  },
};
