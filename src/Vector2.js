class Vector2 {
  constructor(x=0, y=0) {
    this.x = x;
    this.y = y;
  }
  
  Wrap(min, max) {
    this.x = Math.Wrap(this.x, min.x, max.x);
    this.y = Math.Wrap(this.y, min.y, max.y);
  }
  
  Copy() {
    return new Vector2(this.x, this.y);
  }
  
  Equal(v) { return Math.abs(this.x - v.x) < 0.0001 && Math.abs(this.y - v.y) < 0.0001; }
  NotEqual(v) { return !this.Equal(v); }
  
  Add(v) { this.x += v.x; this.y += v.y; return this; }
  Mul(v) { this.x *= v.x; this.y *= v.y; return this; }
  Round() { this.x = Math.round(this.x); this.y = Math.round(this.y); return this; }
  
  Diff(v) { return new Vector2(this.x - v.x, this.y - v.y); }
  Plus(v) { return new Vector2(this.x + v.x, this.y + v.y); }
  Times(c) { return new Vector2(this.x * c, this.y * c); }
  Apply(f) { return new Vector2(f(this.x), f(this.y)); }
  Combine(o, f) { return new Vector2(f(this.x, o.x), f(this.y, o.y)); }
  
  Distance(o) {
    let dx = this.x - o.x;
    let dy = this.y - o.y;
    return Math.sqrt(dx*dx + dy*dy);
  }
  
  get opposite() { return new Vector2(-this.x, -this.y); }
  get rounded() { return this.Apply(Math.round); }
  
  get angle() { return Math.atan2(this.y, this.x); }
  
  static FromAngle(angle) {
    return new Vector2(Math.cos(angle), -Math.sin(angle));
  }
  
  static get zero()  { return new Vector2( 0,  0); }
  static get one()   { return new Vector2( 1,  1); }
  static get left()  { return new Vector2(-1,  0); }
  static get up()    { return new Vector2( 0,  1); }
  static get right() { return new Vector2( 1,  0); }
  static get down()  { return new Vector2( 0, -1); }
};
