jGame.Transform = class {
  constructor() {
    this.position = new Vector2();
    this.rotation = 0;
    this._scale = new Vector2(1, 1);
    this._localScale = new Vector2(1, 1);
    this.snapToPixels = true;
    this._children = new Set();
    this._parent = null;
  }
  
  _addChild(child) {
    this._children.add(child);
  }
  
  Destroy() {
    if (this._parent) {
      this._parent._children.delete(this);
    }
    for (let child of this._children) {
      child.Destroy();
    }
  }
  
  get children() { return this._children; }
  
  get bounds() { return this._bounds; }
  get localBounds() { return this._localBounds; }
  
  get parent() { return this._parent; }
  set parent(p) {
    this._parent = p;
    p._addChild(this);
  }
  
  set scale(newScale) {
    if (typeof newScale == 'number') {
      this._scale = new Vector2(newScale, newScale);
    } else {
      this._scale = newScale;
    }
  }
  get scale() {
    return this._scale;
  }
  
  get matrix() {
    return mat4.multiply(
      mat4.scaling(this._localScale.x, this._localScale.y, 1),
      this.localMatrix);
  }
  
  get localMatrix() {
    var m = mat4.multiply(
      mat4.rotationZ(this.rotation),
      mat4.multiply(
        mat4.scaling(this._scale.x, this._scale.y, 1), 
        mat4.translation(
          this.snapToPixels ? Math.round(this.position.x) : this.position.x,
          this.snapToPixels ? Math.round(this.position.y) : this.position.y, 0)));
    if (this._parent) {
      m = mat4.multiply(m, this._parent.localMatrix);
    }
    return m;
  }
};
