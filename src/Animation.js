class Animation {
  constructor(frames, framerate, loop=true) {
    this.frames = frames;
    this.framerate = framerate;
    this.loop = loop;
    
    var maxFrames = 0;
    for (var f of Object.getOwnPropertyNames(this.frames)) {
      maxFrames = Math.max(maxFrames, this.frames[f].length);
    }
    this.endTime = maxFrames / framerate;
  }
  
  Frame(t) {
    var frame = [];
    for (var f of Object.getOwnPropertyNames(this.frames)) {
      frame.push({
        "property": f,
        "value": this.frames[f][Math.floor(t * this.framerate) % this.frames[f].length],
      });
    }
    return frame;
  }
}
