jGame.Shader = class {
  constructor(vertSource, fragSource, attributes, uniforms) {
    var vertShader = jGame.Shader.Compile(vertSource, jGame.gl.VERTEX_SHADER);
    var fragShader = jGame.Shader.Compile(fragSource, jGame.gl.FRAGMENT_SHADER);
    
    this.program = jGame.gl.createProgram();
    jGame.gl.attachShader(this.program, vertShader);
    jGame.gl.attachShader(this.program, fragShader);
    jGame.gl.linkProgram (this.program);

    if (!jGame.gl.getProgramParameter(this.program, jGame.gl.LINK_STATUS)) {
      console.log ('Could not compile shader from source');
    }
    
    this.attributes = {};
    for (let [name, type] of Object.entries(attributes)) {
      this.attributes[name] = {
        type: type,
        location: jGame.gl.getAttribLocation(this.program, name),
      };
      jGame.gl.enableVertexAttribArray(this.attributes[name].location);
    }
    
    this.uniforms = {};
    for (let [name, type] of Object.entries(uniforms)) {
      this.uniforms[name] = {
        type: type,
        location: jGame.gl.getUniformLocation(this.program, name),
      };
    }
  }
  
  static Compile(source, type) {
    var shader = jGame.gl.createShader(type);
    jGame.gl.shaderSource(shader, source);
    jGame.gl.compileShader(shader);
    
    if (!jGame.gl.getShaderParameter(shader, jGame.gl.COMPILE_STATUS)) {
      console.log(jGame.gl.getShaderInfoLog(shader));
      return null;
    }
    
    return shader;
  }
};
