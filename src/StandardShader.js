jGame.OnInit(() => {
  jGame.StandardShader = new jGame.Shader(
    // Vertex Shader
    `
      precision lowp float;
      
      attribute vec4 vertex;
      attribute vec2 texCoord;
      
      uniform mat4 mvpMatrix;
      uniform vec4 color;
      
      varying vec2 outTexCoord;
      varying vec4 outColor;

      void main() {
        gl_Position = mvpMatrix * vertex;
        outTexCoord = texCoord;
        outColor = color;
      }
    `,
    
    // Fragment Shader
    `
      precision lowp float;
      
      uniform sampler2D texture;
      
      varying vec2 outTexCoord;
      varying vec4 outColor;

      void main() {
        gl_FragColor = outColor * texture2D(texture, outTexCoord);
      }
    `,
    
    // Attributes
    {
      vertex:   'POSITION',
      texCoord: 'TEXCOORD',
    },
    
    // Uniforms
    {
      mvpMatrix: 'MVP_MATRIX',
      color:     'vec4',
      texture:   'sampler2D',
    });
});
