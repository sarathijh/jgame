let Color = {
  Equal: function(c1, c2) {
    return (c1[0] == c2[0]) && (c1[1] == c2[1]) && (c1[2] == c2[2]) && (c1[3] == c2[3]);
  },
  
  RGB:  function(r, g, b) {
    if (g === undefined && b === undefined) {
      b = (r >>  0) & 0xFF;
      g = (r >>  8) & 0xFF;
      r = (r >> 16) & 0xFF;
    }
    return [r/255, g/255, b/255, 1];
  },
  
  ARGB: function(a, r, g, b) {
    if (r === undefined && g === undefined && b === undefined) {
      b = (a >>  0) & 0xFF;
      g = (a >>  8) & 0xFF;
      r = (a >> 16) & 0xFF;
      a = (a >> 24) & 0xFF;
    }
    return [r/255, g/255, b/255, a];
  },
  
  Copy: function(color) {
    return [color[0], color[1], color[2], color[3]];
  },
  
  get black  () { return [0, 0, 0, 1] },
  get white  () { return [1, 1, 1, 1] },
  get red    () { return [1, 0, 0, 1] },
  get green  () { return [0, 1, 0, 1] },
  get blue   () { return [0, 0, 1, 1] },
  get yellow () { return [1, 1, 0, 1] },
  get pink   () { return [1, 0, 1, 1] },
};
