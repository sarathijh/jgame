/**
 * Renders text using characters from a texture atlas.
 * The characters must be the same size
 */
class FixedWidthText extends jGame.GameObject {
  constructor(material, fontInfo={}, text='') {
    super(
      material,
      jGame.gl.createBuffer(),
      jGame.gl.createBuffer(),
      jGame.gl.createBuffer());
    
    if (typeof fontInfo === 'string' && text.length == 0) {
      text = fontInfo;
      fontInfo = {};
    }
    
    this.fontInfo = jGame.Defaults(fontInfo, {
      'prefix': '',
      'letterSpacing': 0,
    });
    
    this._text = '';
    this._meshDirty = false;
    this._hasCharSize = false;
    
    this._halign = "left";
    this._valign = "bottom";
    
    this.text = text;
  }
  
  Align(h, v=h) {
    this._halign = h;
    this._valign = v;
    this._meshDirty = true;
  }
  
  set text(value) {
    if (this._text == value) {
      return;
    }
    this._text = value;
    
    if (!this._hasCharSize) {
      this._calcCharSize();
      this._hasCharSize = true;
    }
    
    this.size.x = this.charWidth * this._text.length + Math.max(this._text.length - 1, 0) * this.fontInfo.letterSpacing;
    this.size.y = this.charHeight;
    
    this._meshDirty = true;
  }
  
  Render(projMatrix) {
    if (this._meshDirty) {
      this._rebuildMesh();
    }
    super.Render(projMatrix);
  }
  
  _calcCharSize() {
    let texture = this.material.texture;
    
    this.charWidth = 0;
    this.charHeight = 0;
    for (var i = 0; i < this._text.length; ++i) {
      if (texture.atlas.hasOwnProperty(this.fontInfo.prefix + this._text[i])) {
        let firstEntry = texture.atlas[this.fontInfo.prefix + this._text[i]];
        this.charWidth = firstEntry.width * (texture ? texture.options.scale : 1);
        this.charHeight = firstEntry.height * (texture ? texture.options.scale : 1);
        break;
      }
    }
  }
  
  _rebuildMesh() {
    var vertices = [];
    var indices = [];
    var texCoords = [];
    
    if (this._text.length > 0) {
      var x = 0;
      var y = 0;
      
      let texture = this.material.texture;
      
      let sw = this.charWidth / this.size.x;
      let sh = this.charHeight / this.size.y;
      
      if (this._halign == "center") {
        x = -0.5;
      } else if (this._halign == "right") {
        x = -1;
      }
      
      if (this._valign == "center") {
        y = -0.5;
      } else if (this._valign == "top") {
        y = -1;
      }
      
      var junkChars = 0;
      for (var i = 0; i < this._text.length; ++i) {
        let c = this._text[i];
        
        let atlasEntry = texture.atlas[this.fontInfo.prefix + c];
        
        if (atlasEntry) {
          vertices.push (...[
            x+ 0,  y+ 0,  0,
            x+ 0,  y+sh,  0,
            x+sw,  y+sh,  0,
            x+sw,  y+ 0,  0,
          ]);
          
          let o = (i - junkChars) * 4;
          indices.push (...[
            o+0, o+2, o+1,
            o+2, o+0, o+3,
          ]);
          
          // TODO: This can be calculated all at once when the atlas is loaded
          // Just calculate the array of coords rather than the OpenGL buffer
          let l = atlasEntry.x / texture.width;
          let r = l + (atlasEntry.width / texture.width);
          let t = atlasEntry.y / texture.height;
          let b = t + (atlasEntry.height / texture.height);
          
          texCoords.push (...[
            l, b,
            l, t,
            r, t,
            r, b,
          ]);
        } else {
          junkChars += 1;
        }
        
        x += sw + this.fontInfo.letterSpacing / this.size.x;
      }
    }
    
    let gl = jGame.gl;
    
    gl.bindBuffer (gl.ARRAY_BUFFER, this.glBuffers.vertices);
    gl.bufferData (gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    this.glBuffers.vertices.itemSize = 3;
    this.glBuffers.vertices.numItems = vertices.length;
    
    gl.bindBuffer (gl.ARRAY_BUFFER, this.glBuffers.texCoords);
    gl.bufferData (gl.ARRAY_BUFFER, new Float32Array(texCoords), gl.STATIC_DRAW);
    this.glBuffers.texCoords.itemSize = 2;
    this.glBuffers.texCoords.numItems = texCoords.length;
    
    gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, this.glBuffers.indicies);
    gl.bufferData (gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
    this.glBuffers.indicies.itemSize = 3;
    this.glBuffers.indicies.numItems = indices.length;
  }
}
