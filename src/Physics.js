let Physics = {};
Physics._colliders = new Set();

Physics.BoxOverlap = function(layer, origin, extents, onHit=null) {
  let l = origin.x - extents.x;
  let r = origin.x + extents.x;
  let t = origin.y + extents.y;
  let b = origin.y - extents.y;
  for (let collider of Physics._colliders) {
    if (!collider.enabled || (layer && layer != collider.layer)) {
      continue;
    }
    if (collider.right  > l && 
        collider.left   < r && 
        collider.top    > b && 
        collider.bottom < t) {
      if (onHit) {
        onHit(collider);
      }
      return collider;
    }
  }
  return false;
}

Physics.BoxCast = function(layer, origin, extents, direction, maxDistance, onHit=null) {
  for (var i = 0; i <= maxDistance; ++i) {
    let collider = Physics.BoxOverlap(layer, new Vector2(origin.x + direction.x * i, origin.y + direction.y * i), extents);
    if (collider) {
      let hit = { "distance": Math.max(i-1, 0), "collider": collider };
      if (onHit) {
        onHit(hit);
      }
      return hit;
    }
  }
  return false;
};

class Collider extends jGame.Transform {
  constructor(x, y, width, height) {
    super();
    this.position.x = x;
    this.position.y = y;
    this.enabled = true;
    this._width = width;
    this._height = height;
    Physics._colliders.add(this);
  }
  
  Destroy() {
    Physics._colliders.delete(this);
  }
  
  get width()  { return this._width  * this.scale.x; }
  get height() { return this._height * this.scale.y; }
  
  set width(v)  { this._width  = v; }
  set height(v) { this._height = v; }
  
  get left()   { return this.position.x - this.width/2; }
  get right()  { return this.position.x + this.width/2; }
  get top()    { return this.position.y + this.height/2; }
  get bottom() { return this.position.y - this.height/2; }
}
