jGame.Shuffle = function(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
};

jGame.Bin = function(x, s) {
  return Math.round(x / s) * s;
};

jGame.Flash = function(t, flashesPerSecond) {
  return !(Math.floor(t * 2 * flashesPerSecond) % 2);
};

jGame.InvokeAfter = function(delay, cb) {
  let timer = {
    _done: false,
    Update: function() {
      if (!this._done) {
        delay -= jGame.deltaTime;
        if (delay <= 0) {
          this.Pause();
          this._done = true;
          cb();
        }
      }
    },
    LateUpdate: function() {},
    Start: function() {
      if (!this._done) {
        jGame._nonRenderedObjects.add(this);
      }
    },
    Pause: function() {
      jGame._nonRenderedObjects.delete(this);
    }
  };
  timer.Start();
  return timer;
};

jGame.Defaults = function(obj, defaults) {
  for (const [key, value] of Object.entries(obj)) {
    defaults[key] = value;
  }
  return defaults;
}

jGame.Require = function(obj, property, type=null) {
  if (obj.hasOwnProperty(property) && (type === null || (obj instanceof type))) {
    return obj[property];
  }
  return null;
}
