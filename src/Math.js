Math.Clamp = function(x, a, b) {
  return Math.min(Math.max(x, a), b);
}

Math.PosMod = function(x, y) {
  return (x >= 0) ? (x % y) : (y - (-x % y));
};

Math.AngularDistance = function(a, b) {
  if (Math.abs(a - b) >= Math.PI) {
    if (a > b) {
      a = Math.NormalizeAngle(a) - 2*Math.PI;
    } else {
      b = Math.NormalizeAngle(b) - 2*Math.PI;
    }
  }
  return Math.abs(b - a);
}
Math.Lerp = function(a, b, t) { return a + (b - a) * Math.Clamp(t, 0, 1); };
Math.NormalizeAngle = function(x) { return Math.PosMod(x + Math.PI, 2*Math.PI) - Math.PI; };
Math.LerpAngle = function(a, b, t) {
  if (Math.abs(a - b) >= Math.PI) {
    if (a > b) {
      a = Math.NormalizeAngle(a) - 2*Math.PI;
    } else {
      b = Math.NormalizeAngle(b) - 2*Math.PI;
    }
  }
  return Math.Lerp(a, b, t);
};

Math.Wrap = function(val, min, max) {
  if (val >= max) { val -= (max-min); }
  if (val <  min) { val += (max-min); }
  return val;
}
