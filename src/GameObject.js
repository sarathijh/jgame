jGame.Material = class {
  constructor(shader, texture=null) {
    this.shader = shader;
    this.texture = texture;
  }
};

jGame.GameObject = class extends jGame.Transform {
  constructor(material, vertexBuffer, indexBuffer, texCoordBuffer) {
    super();
    this.material = material;
    this._color = Color.white;
    this.render = true;
    
    this.animation = null;
    this.animationTime = 0;
    this.animationSpeed = 1;
    this.OnAnimationComplete = null;
    
    this.glBuffers = {
      vertices: vertexBuffer,
      indicies: indexBuffer,
      texCoords: texCoordBuffer,
      };
    
    jGame._renderedObjects.add(this);
  }
  
  get size() { return this._localScale; }
  set size(val) { this._localScale = val; }
  
  get color() { return Color.Copy(this._color); }
  set color(val) { this._color = Color.Copy(val); }
  
  get alpha() { return this._color[3]; }
  set alpha(val) { this._color[3] = Math.Clamp(val, 0, 1); }
  
  Destroy() {
    super.Destroy();
    jGame._renderedObjects.delete(this);
  }
  
  Update() {
    if (this.animation && (this.animation.loop || this.animationTime < this.animation.endTime)) {
      this.animationTime += jGame.deltaTime * this.animationSpeed;
      var done = false;
      if (this.animationTime >= this.animation.endTime) {
        if (this.animation.loop) {
          this.animationTime -= this.animation.endTime;
        } else {
          this.animationTime = this.animation.endTime;
          done = true;
        }
        if (this.OnAnimationComplete) {
          this.OnAnimationComplete();
        }
      }
      if (!done) {
        for (var frame of this.animation.Frame(this.animationTime)) {
          this[frame.property] = frame.value;
        }
      }
    }
  }
  
  LateUpdate() {}
  
  PlayAnimationOnce(anim, onComplete) {
    this.animation = anim;
    this.animationTime = 0;
    this.OnAnimationComplete = () => {
      this.OnAnimationComplete = null;
      onComplete();
    };
  }
  
  Render(projMatrix) {
    if (this.render) {
      if (this.material) {
        let gl = jGame.gl;
        let shader = this.material.shader;
        
        gl.useProgram (shader.program);
        
        for (let [name, attr] of Object.entries(shader.attributes)) {
          switch (attr.type) {
            case 'POSITION':
              gl.bindBuffer(gl.ARRAY_BUFFER, this.glBuffers.vertices);
              gl.vertexAttribPointer(attr.location, this.glBuffers.vertices.itemSize, gl.FLOAT, false, 0, 0);
              break;
            case 'TEXCOORD':
              if (this.glBuffers.texCoords) {
                gl.bindBuffer(gl.ARRAY_BUFFER, this.glBuffers.texCoords);
                gl.vertexAttribPointer(attr.location, this.glBuffers.texCoords.itemSize, gl.FLOAT, false, 0, 0);
              }
              break;
          }
        }
        
        for (let [name, unif] of Object.entries(shader.uniforms)) {
          switch (unif.type) {
            case 'MVP_MATRIX':
              gl.uniformMatrix4fv(unif.location, false, mat4.multiply(this.matrix, projMatrix));
              break;
            case 'sampler2D':
              if (this.material.texture) {
                gl.activeTexture(gl.TEXTURE0);
                gl.bindTexture(gl.TEXTURE_2D, this.material.texture);
                gl.uniform1i(unif.location, 0);
              }
              break;
            case 'vec4': gl.uniform4fv(unif.location, this[name]); break;
          }
        }
        
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.glBuffers.indicies);
        gl.drawElements(gl.TRIANGLES, this.glBuffers.indicies.numItems, gl.UNSIGNED_SHORT, this.glBuffers.indicies);
      }
    }
  }
};
