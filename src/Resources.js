let Resources = {
  _resourcesToLoad: new Set(),
};


// Public:

Resources.Image = function(file) {
  let image = new Image();
  image.file = file;
  
  image._resourceLoader = Resources._imageLoader;
  Resources._resourcesToLoad.add(image);
  
  return image;
};

Resources.Texture = function(file, options={}) {
  let texture = jGame.gl.createTexture();
  
  texture.file = file;
  texture.options = options;
  
  texture.atlas = options.atlas;
  texture.atlas.getSprite = function(key) {
    if (texture.atlas.hasOwnProperty(key)) {
      return texture.atlas[key];
    }
    return null;
  }
  
  texture.width = 0;
  texture.height = 0;
  
  if (!texture.options.hasOwnProperty('scale')) {
    texture.options.scale = 1;
  }
  
  texture._resourceLoader = Resources._textureLoader;
  Resources._resourcesToLoad.add(texture);
  
  return texture;
};

Resources.Load = function(onComplete) {
  Resources._onDoneLoading = onComplete;
  if (!Resources._isDoneLoading()) {
    for (let res of Resources._resourcesToLoad) {
      res._resourceLoader(res);
    }
  }
};


// Private:

Resources._isDoneLoading = function() {
  if (Resources._resourcesToLoad.size == 0 && Resources._onDoneLoading) {
    Resources._onDoneLoading();
  }
  return (Resources._resourcesToLoad.size == 0);
};

Resources._imageLoader = function(image) {
  image.src = image.file;
  image.addEventListener('load', function() {
    let canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    let context = canvas.getContext('2d');
    context.drawImage(image, 0, 0, image.width, image.height);
    let rawPixels = context.getImageData(0, 0, image.width, image.height).data;
    
    var pixels = [];
    var i = 0;
    for (var y = 0; y < image.height; ++y) {
      var row = [];
      for (var x = 0; x < image.width; ++x) {
        row.push([rawPixels[i+0], rawPixels[i+1], rawPixels[i+2]]);
        i += 4;
      }
      pixels.push(row);
    }
    pixels.reverse();
    image.pixels = pixels;
    
    Resources._resourcesToLoad.delete(image);
    Resources._isDoneLoading();
  }, { "once": true });
};

Resources._updateGLBufferFromAtlasInfo = function(texture, name, buffer) {
  let entry = texture.atlas[name];
  
  let l = entry.x / texture.width;
  let r = l + (entry.width / texture.width);
  let t = entry.y / texture.height;
  let b = t + (entry.height / texture.height);
  
  let texCoordData = [
    l, b,
    l, t,
    r, t,
    r, b,
  ];
  
  let gl = jGame.gl;
  
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texCoordData), gl.STATIC_DRAW);
  buffer.itemSize = 2;
  buffer.numItems = texCoordData.length;
};

Resources._textureLoader = function(texture) {
  var image = new Image();
  image.src = texture.file;
  image.addEventListener('load', function() {
    let gl = jGame.gl;
    
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    
    gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
    
    if (texture.options.wrap == 'repeat') {
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    } else {
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }
    
    if (texture.options.filter == 'linear') {
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    } else {
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    }
    
    texture.image = image;
    texture.width = this.width;
    texture.height = this.height;
    
    let slicingParams = texture.options.slicingParams;
    if (slicingParams) {
      texture.atlas = {};
      var tileWidth = slicingParams.tileWidth;
      var tileHeight = slicingParams.tileHeight;
      var tilesX = slicingParams.tilesX;
      var tilesY = slicingParams.tilesY;
      if (tilesX) {
        tileWidth = Math.floor((texture.width - 2*slicingParams.padding - (tilesX-1)*slicingParams.tilePadding) / tilesX);
        tileHeight = Math.floor((texture.height - 2*slicingParams.padding - (tilesY-1)*slicingParams.tilePadding) / tilesY);
      } else if (tileWidth) {
        tilesX = Math.floor((texture.width - 2*slicingParams.padding + slicingParams.tilePadding) / (tileWidth + slicingParams.tilePadding));
        tilesY = Math.floor((texture.height - 2*slicingParams.padding + slicingParams.tilePadding) / (tileHeight + slicingParams.tilePadding));
      } else {
        console.log('Slicing params for texture must either define tilesX,tilesY or tileWidth,tileHeight');
      }
      var n = 0;
      for (var i = 0; i < tilesY; ++i) {
        for (var j = 0; j < tilesX; ++j) {
          texture.atlas[n] = {
            x: slicingParams.padding + j*(tileWidth + slicingParams.tilePadding),
            y: slicingParams.padding + i*(tileHeight + slicingParams.tilePadding),
            width: tileWidth,
            height: tileHeight,
          };
          ++n;
        }
      }
    }
    
    if (texture.atlas) {
      let sharedBuffers = {};
      
      for (let name of Object.keys(texture.atlas)) {
        let entry = texture.atlas[name];
        
        if (entry.noBuffer) {
          continue;
        }
        
        if (entry.sharedBuffer && sharedBuffers.hasOwnProperty(entry.sharedBuffer)) {
          entry.texCoordBuffer = sharedBuffers[entry.sharedBuffer];
          continue;
        }
        
        let l = entry.x / texture.width;
        let r = l + (entry.width / texture.width);
        let t = entry.y / texture.height;
        let b = t + (entry.height / texture.height);
        
        texture.atlas[name].uvRect = {
          minX: l,
          minY: t,
          maxX: r,
          maxY: b,
        };
        
        entry.texCoordBuffer = gl.createBuffer();
        Resources._updateGLBufferFromAtlasInfo(texture, name, entry.texCoordBuffer);
        
        if (entry.sharedBuffer) {
          sharedBuffers[entry.sharedBuffer] = entry.texCoordBuffer;
        }
      }
      
      texture.sharedBuffers = sharedBuffers;
    }
    
    if (!texture.atlas) { texture.atlas = {}; }
    if (!texture.atlas.hasOwnProperty("full")) {
      texture.atlas["full"] = {
        texCoordBuffer: Sprite.FullSpriteTexCoordBuffer,
        x: 0,
        y: 0,
        width: this.width,
        height: this.height,
        uvRect: {
          minX: 0,
          minY: 0,
          maxX: 1,
          maxY: 1,
        },
      };
    }
    
    Resources._resourcesToLoad.delete(texture);
    Resources._isDoneLoading();
  }, { "once": true });
};
