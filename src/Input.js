let Input = {
  mousePosition: Vector2.zero,
  
  _keyDown: {},
  _keyPressedThisFrame: {},
  
  keyDown: function (k) {
    return this._keyDown [k];
  },
  
  keyPressedThisFrame: function (k) {
    return this._keyPressedThisFrame [k];
  },
  
  _onBeforeFrame: function () {
    // For all keys that are down, set that they were pressed this frame if
    // the keyPressedThisFrame object doesn't have that key set.
    // These keys will be set to false after the first frame until the key is
    // released, after which they will be deleted.
    for (var key of Object.keys (this._keyDown)) {
      if (! this._keyPressedThisFrame.hasOwnProperty (key)) {
        this._keyPressedThisFrame [key] = true;
      }
    }
  },
  
  _onAfterFrame: function () {
    for (var key of Object.keys (this._keyPressedThisFrame)) {
      this._keyPressedThisFrame [key] = false;
    }
  },
};

Input.GetAxis = function(a) {
  if (a == "Horizontal") {
    let right = Input.keyDown(jGame.Key.RIGHT);
    let left = Input.keyDown(jGame.Key.LEFT);
    return right && !left ? 1 : left && !right ? -1 : 0;
  }
  if (a == "Vertical") {
    let up = Input.keyDown(jGame.Key.UP);
    let down = Input.keyDown(jGame.Key.DOWN);
    return up && !down ? 1 : down && !up ? -1 : 0;
  }
}

document.addEventListener('keydown', e => {
  Input._keyDown[e.keyCode] = true;
});

document.addEventListener('keyup', e => {
  delete Input._keyDown[e.keyCode];
  delete Input._keyPressedThisFrame[e.keyCode];
});

window.addEventListener('blur', e => {
  Input._keyDown = {};
  Input._keyPressedThisFrame = {};
});

jGame.OnInit(() => {
  jGame.canvas.addEventListener('mousemove', e => {
    let rect = canvas.getBoundingClientRect();
    Input.mousePosition.x = e.clientX - rect.left;
    Input.mousePosition.y = e.clientY - rect.top;
  });
});

Input.Key = {
  "ENTER": 13,
  "LEFT": 37,
  "UP": 38,
  "RIGHT": 39,
  "DOWN": 40,
};
