class Sprite extends jGame.GameObject {
  constructor(material, props={}) {
    super(material, Sprite.VertexBuffer, Sprite.IndexBuffer);
    
    this._flipX = false;
    this._flipY = false;
    
    if (typeof props === 'string') {
      this.sprite = props;
    } else {
      for (let [name, value] of Object.entries(props)) {
        this[name] = value;
      }
    }
  }
  
  set flipX(val) { if (this._flipX != val) { this._flipX = val; this._scale.x *= -1; }}
  set flipY(val) { if (this._flipY != val) { this._flipY = val; this._scale.y *= -1; }}
  
  set sprite(val) {
    if (this.material.texture) {
      this._sprite = val;
      
      let atlasEntry = typeof val === 'string' ? this.material.texture.atlas[val] : val;
      this.glBuffers.texCoords = atlasEntry.texCoordBuffer;
      this.size.x = atlasEntry.width  * (this.material.texture ? this.material.texture.options.scale : 1);
      this.size.y = atlasEntry.height * (this.material.texture ? this.material.texture.options.scale : 1);
    }
  }
  get sprite() { return this._sprite; }
  
  get topLeft()     { return new Vector2(this.position.x - this.size.x/2, this.position.y + this.size.y/2); }
  get topRight()    { return new Vector2(this.position.x + this.size.x/2, this.position.y + this.size.y/2); }
  get bottomLeft()  { return new Vector2(this.position.x - this.size.x/2, this.position.y - this.size.y/2); }
  get bottomRight() { return new Vector2(this.position.x + this.size.x/2, this.position.y - this.size.y/2); }
  get extents()     { return new Vector2(this.size.x/2, this.size.y/2); }
}

jGame.OnInit(() => {
  let vertices = [
    -0.5,  -0.5,  0,
    -0.5,  +0.5,  0,
    +0.5,  +0.5,  0,
    +0.5,  -0.5,  0,
  ];
  
  let indices = [
    0, 2, 1,
    2, 0, 3,
  ];
  
  let texCoords = [
    0, 1,
    0, 0,
    1, 0,
    1, 1,
  ];
  
  let gl = jGame.gl;
  
  Sprite.VertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, Sprite.VertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
  Sprite.VertexBuffer.itemSize = 3;
  Sprite.VertexBuffer.numItems = vertices.length;
  
  Sprite.IndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, Sprite.IndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
  Sprite.IndexBuffer.itemSize = 3;
  Sprite.IndexBuffer.numItems = indices.length;
  
  Sprite.FullSpriteTexCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, Sprite.FullSpriteTexCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texCoords), gl.STATIC_DRAW);
  Sprite.FullSpriteTexCoordBuffer.itemSize = 2;
  Sprite.FullSpriteTexCoordBuffer.numItems = texCoords.length;
});
