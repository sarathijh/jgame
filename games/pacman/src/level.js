class Level extends jGame.GameObject {
  constructor(map) {
    super();
    this.map = map;
    this.size.x = (map.width  - 2) * Level.WallSize;
    this.size.y = (map.height - 2) * Level.WallSize;
    
    this.ghostPositions = [];
    this.bigDots = [];
    this.walls = [];
    this.dotCount = 0;
    
    for (var y = 0; y < this.map.height; ++y) {
      for (var x = 0; x < this.map.width; ++x) {
        let pixel = this.map.pixels[y][x];
        
        let cx = (x-1) * Level.WallSize + (2 * Res.Scale) + ( 4 * Res.Scale);
        let cy = (y-1) * Level.WallSize + (2 * Res.Scale) + Res.HudHeight;
        
        if (Color.Equal(pixel, [  0,   0, 255])) {
          this.CreateWall(cx, cy, x, y);
        } else {
          cx += 2 * Res.Scale;
          cy -= 2 * Res.Scale;
          if (Color.Equal(pixel, [255, 255, 255])) { this.CreateDot(cx, cy, "dot"); } else
          if (Color.Equal(pixel, [255,   0,   0])) { this.CreateDot(cx, cy, "dot_big_0"); } else
          if (Color.Equal(pixel, [255, 255,   0])) { this.playerStartPosition = new Vector2(cx, cy); }
          if (Color.Equal(pixel, [  0, 255,   0])) { this.ghostPositions.push(new Vector2(cx, cy)); }
          if (Color.Equal(pixel, [255,   0, 255])) { this.readyPosition = new Vector2(cx, cy); }
        }
      }
    }
  }
  
  CreateDot(cx, cy, sprite) {
    let dot = new Sprite(Res.Material, sprite);
    dot.parent = this;
    dot.position = new Vector2(cx, cy);
    
    if (sprite == "dot_big_0") {
      dot.animation = Res.AnimDotBig;
      dot.big = true;
      this.bigDots.push(dot);
    }
    
    let collider = new Collider(cx, cy, 2, 2);
    collider.parent = this;
    collider.layer = "dot";
    collider.dot = dot;
    
    this.dotCount += 1;
  }
  
  CreateWall(cx, cy, mx, my) {
    var t = (my == 0)                   || Color.Equal(this.map.pixels[my+1][mx  ], [0, 0, 255]);
    var l = (mx == 0)                   || Color.Equal(this.map.pixels[my  ][mx-1], [0, 0, 255]);
    var r = (mx == this.map.width  - 1) || Color.Equal(this.map.pixels[my  ][mx+1], [0, 0, 255]);
    var b = (my == this.map.height - 1) || Color.Equal(this.map.pixels[my-1][mx  ], [0, 0, 255]);
    
    var spriteName = null;
    var angle = 0;
    if (t + l + r + b == 2) {
      if (((t && r) || (t && l) || (b && r) || (b && l))) {
        spriteName = "wall_corner";
        if (t && r) { angle =  Math.PI/2; } else
        if (t && l) { angle =  Math.PI; } else
        if (b && l) { angle = -Math.PI/2; }
      }
      else if ((t && b) || (r && l)) {
        spriteName = "wall_straight";
        if (t && b) { angle = Math.PI/2; }
      }
    }
    else if (t + l + r + b == 3) {
      spriteName = "wall_single";
      if (!b) { angle =  Math.PI/2; } else
      if (!r) { angle =  Math.PI; } else
      if (!t) { angle = -Math.PI/2; }
    }
    else if (t + l + r + b == 1) {
      spriteName = "wall_end";
      if (b) { angle =  Math.PI/2; } else
      if (r) { angle =  Math.PI; } else
      if (t) { angle = -Math.PI/2; }
    }
    
    if (spriteName) {
      let wall = new Sprite(Res.Material, spriteName);
      wall.parent = this;
      wall.position = new Vector2(cx, cy);
      wall.rotation = angle;
      wall.color = Level.WallColor;
      this.walls.push(wall);
    }
    
    if (t + l + r + b != 0) {
      let collider = new Collider(cx, cy, Level.WallSize, Level.WallSize);
      collider.parent = this;
      collider.layer = "wall";
    }
  }
}

Level.WallSize = 4 * Res.Scale;
Level.WallColor = Color.RGB(0x0078f8);
