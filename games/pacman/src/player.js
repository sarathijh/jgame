class Player extends Actor {
  constructor(startPosition) {
    super();
    this.position = startPosition;
    this.rawRotation = 0;
    this.targetRotation = 0;
    
    this.hitboxSize = this.extents.Times(0.25);
  }
  
  Appear() {
    this.sprite = "pacman0";
  }
  
  PlayDeathAnimation(onComplete) {
    this.rotation = Math.PI/2;
    this.animationSpeed = 1;
    this.PlayAnimationOnce(Res.AnimDead, onComplete);
  }
  
  Update() {
    super.Update();
    
    // If we're in the "game over" or "user paused" state, then do nothing.
    if (game.IsGameOver || game.IsUserPaused) { return; }
    
    // If the player presses a directional key, then set pacman's next turn
    // to the corresponding direction.
    if (Input.keyPressedThisFrame(Input.Key.LEFT))  { this.nextTurn = Vector2.left;  } else
    if (Input.keyPressedThisFrame(Input.Key.UP))    { this.nextTurn = Vector2.up;    } else
    if (Input.keyPressedThisFrame(Input.Key.RIGHT)) { this.nextTurn = Vector2.right; } else
    if (Input.keyPressedThisFrame(Input.Key.DOWN))  { this.nextTurn = Vector2.down;  }
    
    // If we're in the "paused" state, then allow changing turning direction,
    // but don't move yet.
    if (game.IsPaused) { return; }
    
    // Perform the movement logic from the parent Actor class.
    this.Move();
    
    // If we have a direction (we started moving), then adjust the animation
    // and rotation of the sprite.
    if (this.direction) {
      this.animation = this.hitWall ? Res.AnimIdle : Res.AnimMove;
      if (this.direction.Equal(Vector2.left)) {
        this.targetRotation = 0;
        this.flipX = true;
      } else {
        this.targetRotation = this.direction.angle;
        if (this.direction.Equal(Vector2.right)) { this.flipX = false; }
      }
      this.rawRotation = Math.LerpAngle(this.rawRotation, this.targetRotation, jGame.deltaTime * 20);
      this.rotation = this.rawRotation;
    }
    
    // Eat a dot if pacman collides with it.
    Physics.BoxOverlap("dot", this.position.rounded, this.hitboxSize, collider => {
      collider.dot.Destroy();
      collider.Destroy();
      game.OnDotEaten(collider.dot.big);
    });
    
    // Let the ghost handle collisions with the player, because the ghost
    // knows whether it is in a scared state or not.
    Physics.BoxOverlap("ghost", this.position.rounded, this.hitboxSize, collider => {
      collider.ghost.OnPlayerCollision();
    });
  }
}
