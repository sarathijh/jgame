class CoffeeBreak extends jGame.GameObject {
  constructor() {
    super();
    
    let GhostDistance   = 32 * Res.Scale;
    let GhostSeparation = 18 * Res.Scale;
    let Padding         = 32 * Res.Scale;
    
    this.pacman = new Sprite(Res.Material, {
      parent:    this,
      sprite:    "pacman0",
      animation: Res.AnimMove,
      flipX:     true,
      });
    
    for (var i = 0; i < 4; ++i) {
      let ghost = new Sprite(Res.Material, {
        parent:    this,
        sprite:    "ghost0",
        animation: Res.AnimGhost,
        color:     Ghost.Colors[i],
        });
      ghost.position.x = this.pacman.position.x + GhostDistance + i * GhostSeparation;
      
      let eyes = new Sprite(Res.Material, {
        parent: ghost,
        sprite: "ghost_eyes_l",
        });
    }
    
    this.position.x = jGame.canvas.width + Padding;
    this.position.y = jGame.canvas.height/2 + Res.HudHeight;
    
    this.speed = (jGame.canvas.width + 2*Padding + GhostDistance + 3*GhostSeparation) / Res.MusicCoffeeBreak.duration();
  }
  
  /* override */ Update() {
    this.position.x -= this.speed * jGame.deltaTime;
  }
}
