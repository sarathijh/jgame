let Res = {};

Res.Scale = 1;
Res.HudHeight = 20 * Res.Scale;

jGame.OnInit(() => {
  Res.Level0 = Resources.Image("levels/LevelClassic.png");
  
  var atlas = {
    "logo"                   : { x:   1, y: 69, width: 182, height: 46 },
    
    "poof"                   : { x: 239, y:  1, width:  16, height: 16 },
    
    "ghost0"                 : { x:   1, y: 18, width:  16, height: 16 },
    "ghost1"                 : { x:  18, y: 18, width:  16, height: 16 },
    "ghost_eyes_r"           : { x:  35, y: 18, width:  16, height: 16 },
    "ghost_eyes_l"           : { x:  52, y: 18, width:  16, height: 16 },
    "ghost_eyes_u"           : { x:  69, y: 18, width:  16, height: 16 },
    "ghost_eyes_d"           : { x:  86, y: 18, width:  16, height: 16 },
    "ghost_eyes_scared"      : { x: 103, y: 18, width:  16, height: 16 },
    "ghost_eyes_returning_r" : { x: 120, y: 18, width:  16, height: 16 },
    "ghost_eyes_returning_l" : { x: 137, y: 18, width:  16, height: 16 },
    "ghost_eyes_returning_u" : { x: 154, y: 18, width:  16, height: 16 },
    "ghost_eyes_returning_d" : { x: 171, y: 18, width:  16, height: 16 },
    
    "dot"                    : { x:   1, y: 35, width:  16, height: 16 },
    "dot_big_0"              : { x:  18, y: 35, width:   8, height:  8 },
    "dot_big_1"              : { x:  26, y: 43, width:   8, height:  8 },
    
    "wall_corner"            : { x:  35, y: 35, width:   4, height:  4 },
    "wall_straight"          : { x:  40, y: 35, width:   4, height:  4 },
    "wall_single"            : { x:  35, y: 40, width:   4, height:  4 },
    "wall_end"               : { x:  40, y: 40, width:   4, height:  4 },
    "wall_center"            : { x:  35, y: 45, width:   4, height:  4 },
    
    "score_200"              : { x:  52, y: 44, width:  16, height:  8 },
    "score_400"              : { x:  69, y: 44, width:  16, height:  8 },
    "score_800"              : { x: 103, y: 35, width:  16, height:  8 },
    "score_1600"             : { x: 120, y: 35, width:  16, height:  8 },
  };
  
  // Pacman
  for (var i = 0; i < 14; ++i) { atlas["pacman" + i] = { x: 1 + i * 17, y: 1, width: 16, height: 16 }; }
  // Digits
  for (var i = 0; i < 10; ++i) { atlas[String.fromCharCode("0".charCodeAt(0) + i)] = { x: 1+i*8, y: 52, width: 8, height: 8 }; }
  // Uppercase Letters
  for (var i = 0; i < 26; ++i) { atlas[String.fromCharCode("A".charCodeAt(0) + i)] = { x: 1+i*8, y: 60, width: 8, height: 8 }; }
  // Punctuation
  let punc = "-/.>`\"!";
  for (var i = 0; i < punc.length; ++i) { atlas[punc[i]] = { x: 80 + i * 8, y: 52, width: 8, height: 8 }; }
  
  Res.SpriteSheet = Resources.Texture("textures/Pacman.png", {
    "atlas": atlas,
    "scale": Res.Scale,
  });
  Res.Material = new jGame.Material(jGame.StandardShader, Res.SpriteSheet);
  
  Res.AnimIdle   = new Animation({"sprite": ["pacman1"]}, 0);
  Res.AnimMove   = new Animation({"sprite": ["pacman0", "pacman1", "pacman2", "pacman1"]}, 20);
  Res.AnimDead   = new Animation({"sprite": ["pacman2", "pacman2", "pacman3", "pacman4", "pacman5", "pacman6", "pacman7", "pacman8", "pacman9", "pacman10", "pacman11", "pacman12", "pacman13"]}, 9, false);
  Res.AnimDotBig = new Animation({"sprite": ["dot_big_0", "dot_big_1"]}, 4);
  Res.AnimGhost  = new Animation({"sprite": ["ghost0", "ghost1"]}, 10);
  Res.AnimFlash  = new Animation({"color": [Color.white, Color.black]}, 4);
  
  Res.MusicBegin           = new Howl({ src: ["audio/PacmanStartMusic.ogg"], });
  Res.MusicSiren           = new Howl({ src: ["audio/PacmanGhostNormalMove.ogg"], loop: true, });
  Res.MusicGhostScared     = new Howl({ src: ["audio/PacmanGhostTurnBlue.ogg"],   loop: true, });
  Res.MusicGhostReturnHome = new Howl({ src: ["audio/PacmanGhostReturnHome.ogg"], loop: true, });
  Res.MusicCoffeeBreak     = new Howl({ src: ["audio/PacmanCoffeeBreak.ogg"],     loop: false, });
  
  Res.SfxEatGhost =  new Howl({ src: ["audio/PacmanEatGhost.ogg"], });
  Res.SfxGameOver =  new Howl({ src: ["audio/PacmanMiss.ogg"], });
  Res.SfxChomp    = [new Howl({ src: ["audio/PacmanChomp1.ogg"], pool: 1, }),
                     new Howl({ src: ["audio/PacmanChomp2.ogg"], pool: 1, })];
});
