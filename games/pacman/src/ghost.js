class Ghost extends Actor {
  constructor(player) {
    super();
    this.player = player;
    this.speed -= 5 * Res.Scale;
    this.turnTimer = 0;
    this.scared = false;
    this.returningHome = false;
    
    this.eyes = new Sprite(Res.Material, "ghost_eyes_d");
    this.eyes.parent = this;
    this.eyes.render = false;
    
    this.collider = new Collider(0, 0, this.extents.x, this.extents.y);
    this.collider.layer = "ghost";
    this.collider.ghost = this;
    
    this.direction = this.RandomTurn();
  }
  
  Appear() {
    this.color = this.ghostColor;
    this.animation = Res.AnimGhost;
    this.eyes.render = true;
  }
  
  OnScared() {
    if (this.returningHome) { return; }
    if (!this.scared) {
      this.speed /= 2;
      this.scared = true;
      this.color = Ghost.ScaredColor;
      this.eyes.color = Ghost.ScaredEyeColor;
      this.SetEyes();
    }
  }
  
  OnUnscared() {
    if (this.scared) {
      this.speed *= 2;
      this.scared = false;
      this.color = this.ghostColor;
      this.eyes.color = Color.white;
      this.SetEyes();
    }
  }
  
  ResetTurnTimer() {
    this.turnTimer = 1 + Math.random();
  }
  
  RandomTurn() {
    var options = [];
    for (let dir of jGame.Shuffle([Vector2.left, Vector2.up, Vector2.right, Vector2.down])) {
      let sp = this.position.rounded;
      if (!Physics.BoxCast("wall", sp, this.extents, dir, 1)) {
        options.push([sp, dir]);
      }
    }
    if (options.length == 1) {
      this.position = options[0][0];
      return options[0][1];
    }
    if (options.length >= 2) {
      for (let [sp, dir] of options) {
        if (!this.direction || dir.NotEqual(this.direction.opposite) || Math.random() > 0.75) {
          this.position = sp;
          return dir;
        }
      }
    }
    return null;
  }
  
  OnPlayerCollision() {
    if (this.scared) {
      this.collider.enabled = false;
      this.returningHome = true;
      this.render = false;
      this.eyes.render = false;
      game.OnGhostEaten(this);
    } else {
      game.OnPlayerEaten();
    }
  }
  
  ReturnHome() {
    this.speed *= 2;
    this.eyes.render = true;
    this.OnUnscared();
  }
  
  /* override */ Destroy() {
    super.Destroy();
    this.eyes.Destroy();
    this.collider.Destroy();
  }
  
  SetEyes() {
    if (this.scared) {
      this.eyes.sprite = "ghost_eyes_scared";
    } else if (this.direction) {
      if (this.returningHome) {
        if (this.direction.Equal(Vector2.right)) { this.eyes.sprite = "ghost_eyes_returning_r"; } else
        if (this.direction.Equal(Vector2.left))  { this.eyes.sprite = "ghost_eyes_returning_l"; } else
        if (this.direction.Equal(Vector2.up))    { this.eyes.sprite = "ghost_eyes_returning_u"; } else
        if (this.direction.Equal(Vector2.down))  { this.eyes.sprite = "ghost_eyes_returning_d"; }
      } else {
        if (this.direction.Equal(Vector2.right)) { this.eyes.sprite = "ghost_eyes_r"; } else
        if (this.direction.Equal(Vector2.left))  { this.eyes.sprite = "ghost_eyes_l"; } else
        if (this.direction.Equal(Vector2.up))    { this.eyes.sprite = "ghost_eyes_u"; } else
        if (this.direction.Equal(Vector2.down))  { this.eyes.sprite = "ghost_eyes_d"; }
      }
    }
  }
  
  TurnToward(targetPos) {
    var angle = targetPos.Diff(this.position).angle;
    let turnDir = Vector2.FromAngle(jGame.Bin(angle, Math.PI/2));
    this.nextTurn = turnDir;
  }
  
  TurnAwayFrom(targetPos) {
    var angle = this.position.Diff(targetPos).angle;
    let turnDir = Vector2.FromAngle(jGame.Bin(angle, Math.PI/2));
    if (turnDir.NotEqual(this.direction.opposite)) {
      this.nextTurn = turnDir;
    }
  }
  
  /* override */ Update() {
    super.Update();
    
    if (game.IsGameOver) {
      return;
    }
    
    if (game.IsPaused) {
      this.animationSpeed = 0;
      return;
    }
    this.animationSpeed = 1;
    
    this.Move();
    
    let distToPlayer = this.position.Distance(this.player.position);
    
    if (this.returningHome || this.scared) {
      this.TurnAwayFrom(this.player.position);
    } else if (distToPlayer <= 50 * Res.Scale) {
      if (Math.random() > 0.6) {
        this.TurnToward(this.player.position);
      } else {
        for (let dir of jGame.Shuffle([Vector2.left, Vector2.up, Vector2.right, Vector2.down])) {
          if (!this.direction || (this.direction.NotEqual(dir) && dir.NotEqual(this.direction.opposite))) {
            this.nextTurn = dir;
            break;
          }
        }
      }
    } else {
      this.turnTimer -= jGame.deltaTime;
      if (this.turnTimer <= 0) {
        if (this.nextTurn) {
          for (let dir of jGame.Shuffle([Vector2.left, Vector2.up, Vector2.right, Vector2.down])) {
            if (this.direction.NotEqual(dir) && (dir.NotEqual(this.direction.opposite) || Math.random() > 0.75)) {
              this.nextTurn = dir;
              break;
            }
          }
        } else {
          this.nextTurn = this.RandomTurn();
        }
        this.ResetTurnTimer();
      }
    }
    
    if (this.hitWall) {
      this.direction = this.RandomTurn();
      this.nextTurn = this.direction;
      this.ResetTurnTimer();
    }
    
    if (this.returningHome) {
      if (distToPlayer >= 125 * Res.Scale) {
        this.returningHome = false;
        this.render = true;
        this.speed /= 2;
        this.collider.enabled = true;
      }
    }
    
    if (this.scared) {
      if (game.scaredTimer <= 0) {
        this.OnUnscared();
      } else if (game.scaredTimer <= 1.5) {
        let i = Math.floor(game.scaredTimer*5)%2;
        this.color = [Ghost.ScaredFlashColor, Ghost.ScaredColor][i];
        this.eyes.color = [Ghost.ScaredFlashEyeColor, Ghost.ScaredEyeColor][i];
      }
    } else {
      this.SetEyes();
    }
  }
  
  /* override */ LateUpdate() {
    super.LateUpdate();
    this.collider.position = this.position.rounded;
  }
}

Ghost.Colors = [
  Color.RGB(0xab1300), // Red
  Color.RGB(0xd8b8f8), // Pink
  Color.RGB(0x3fbfff), // Blue
  Color.RGB(0xe75f13), // Orange
  ];

Ghost.ScaredColor         = Color.RGB(0x0078f8);
Ghost.ScaredEyeColor      = Color.RGB(0xd8b8f8);
Ghost.ScaredFlashColor    = Color.RGB(0xffffff);
Ghost.ScaredFlashEyeColor = Color.RGB(0xff0000);
