class PacmanGame extends jGame.GameObject {
  constructor() {
    super();
    this.IsInGame = false;
    this.IsPaused = false;
    this.IsGameOver = false;
    this.IsGhostReturningHome = false;
    this.AreGhostsScared = false;
    this.flashing = false;
    this.gotOneUp = false;
    
    if (jGame.Init("#canvas", 256 * Res.Scale, 240 * Res.Scale, Color.black)) {
      Resources.Load(() => this._onDoneLoading());
    }
  }
  
  _onDoneLoading() {
    this.titleScreen = new TitleScreen();
    this.titleScreen.delegate = this;
    
    Res.MusicBegin.on("end", () => this._beginLevel());
    Res.MusicCoffeeBreak.on("end", () => this._onCoffeeBreakDone());
  }
  
  OnTitleSelect(option) {
    if (option == "START") {
      this.titleScreen.Destroy();
      this.timer = jGame.InvokeAfter(0.2, () => this._newGame());
    }
  }
  
  _createLevel() {
    this.level = new Level(Res.Level0);
    jGame.canvas.setSize(this.level.size.x + 8 * Res.Scale, this.level.size.y + 24 * Res.Scale);
  }
  
  _createHud() {
    this.hud = new jGame.Transform();
    this.hud.position = (new Vector2(0, 2)).Times(Res.Scale);
    
    this.score = 0;
    this.scoreText = new FixedWidthText(Res.Material, "SCORE 0");
    this.scoreText.parent = this.hud;
    this.scoreText.Align("left", "center");
    this.scoreText.position = (new Vector2(4, 7)).Times(Res.Scale);
    
    this.oneUp = new FixedWidthText(Res.Material, "1UP");
    this.oneUp.parent = this.hud;
    this.oneUp.Align("right", "center");
    this.oneUp.animation = Res.AnimFlash;
    this.oneUp.render = false;
    this.oneUp.position = (new Vector2(0, 7)).Times(Res.Scale);
    
    this.lives = [];
    for (var i = 0; i < 2; ++i) { this._addLife(); }
  }
  
  _createActors() {
    this.player = new Player(this.level.playerStartPosition);
    
    this.ghosts = new Set();
    for (var i = 0; i < 4; ++i) {
      let ghost = new Ghost(this.player);
      ghost.position = this.level.ghostPositions[i];
      ghost.ghostColor = Ghost.Colors[i];
      this.ghosts.add(ghost);
    }
    
    this.timer = jGame.InvokeAfter(0.15, () => {
      this.player.Appear();
      for (let ghost of this.ghosts) { ghost.Appear(); }
    });
  }
  
  _newGame() {
    this.IsPaused = true;
    
    this._createLevel();
    this._createHud();
    
    this.pausedText = new FixedWidthText(Res.Material, "PAUSED");
    this.pausedText.Align("center");
    this.pausedText.position = this.level.readyPosition;
    this.pausedText.color = Color.RGB(0xffa347);
    this.pausedText.render = false;
    
    this.readyText = new FixedWidthText(Res.Material, "READY!");
    this.readyText.Align("center");
    this.readyText.position = this.level.readyPosition;
    this.readyText.color = Color.RGB(0xffa347);
    
    this.timer = jGame.InvokeAfter(1.5, () => this._createActors());
    
    Res.MusicBegin.play();
  }
  
  _beginLevel() {
    this.IsInGame = true;
    this.IsPaused = false;
    this.readyText.render = false;
    this.chompSound = 0;
    Res.MusicSiren.play();
  }
  
  _addLife() {
    let life = new Sprite(Res.Material, "pacman1");
    life.parent = this.hud;
    life.position = new Vector2(jGame.canvas.width - (5 + 16*this.lives.length + 4) * Res.Scale, 8 * Res.Scale);
    this.lives.push(life);
  }
  
  _addOneUp() {
    if (this.gotOneUp) {
      this.gotOneUp = false;
      this.oneUp.render = false;
      this._addLife();
    }
  }
  
  _restartLevel(loseLife=false) {
    this.player.Destroy();
    this.IsGameOver = false;
    this.IsPaused = true;
    this.IsGhostReturningHome = false;
    
    this._addOneUp();
    
    if (loseLife) {
      if (this.lives.length > 0) {
        this.lives.pop().Destroy();
      } else {
        let gameOverText = new FixedWidthText(Res.Material, "GAME  OVER");
        gameOverText.color = Color.red;
        gameOverText.Align("center");
        gameOverText.position = this.level.readyPosition;
        return;
      }
    }
    
    this.readyText.render = true;
    this._createActors();
    this.timer = jGame.InvokeAfter(2, () => this._beginLevel());
  }
  
  _onCoffeeBreakDone() {
    this.coffeeBreak.Destroy();
    this.coffeeBreak = null;
    this.timer = jGame.InvokeAfter(1, () => {
      this._createLevel();
      this._restartLevel();
    });
  }
  
  _addScore(amount) {
    let broke10k = (Math.floor(this.score / 10000) != (Math.floor((this.score + amount) / 10000)));
    if (this.lives.length <= 6 && broke10k && !this.gotOneUp) {
      this.gotOneUp = true;
      if (this.lives.length == 0) {
        this.oneUp.position.x = jGame.canvas.width - 4 * Res.Scale;
      } else {
        this.oneUp.position.x = this.lives[this.lives.length-1].position.x - (8 + 4) * Res.Scale;
      }
      this.oneUp.animationTime = 0;
      this.oneUp.render = true;
    }
    this.score += amount;
    this.scoreText.text = "SCORE " + this.score;
  }
  
  _winLevel() {
    this.IsInGame = false;
    this.IsPaused = true;
    this.AreGhostsScared = false;
    this.player.animationTime = 0;
    this.player.animationSpeed = 0;
    Res.MusicSiren.stop();
    Res.MusicGhostScared.stop();
    Res.MusicGhostReturnHome.stop();
    
    this.timer = jGame.InvokeAfter(1.5, () => {
      for (let ghost of this.ghosts) { ghost.Destroy(); }
      this.flashing = true;
      this.flashTimer = 2;
    });
  }
  
  _scareGhosts() {
    if (this.AreGhostsScared) { return; }
    this.AreGhostsScared = true;
    this.scaredTimer = 5;
    this.ghostPoints = 200;
    for (let ghost of this.ghosts) { ghost.OnScared(); }
    Res.MusicSiren.stop();
    if (!this.IsGhostReturningHome) {
      Res.MusicGhostScared.play();
    }
  }
  
  _unscareGhosts() {
    if (!this.AreGhostsScared) { return; }
    this.AreGhostsScared = false;
    Res.MusicGhostScared.stop();
    if (!this.IsGhostReturningHome) {
      Res.MusicSiren.play();
    }
  }
  
  OnDotEaten(big) {
    if (big) {
      this._addScore(50);
      this._scareGhosts();
    } else {
      this._addScore(10);
      Res.SfxChomp[this.chompSound].play();
      this.chompSound = (this.chompSound+1) % Res.SfxChomp.length;
    }
    this.level.dotCount -= 1;
    if (this.level.dotCount == 0) {
      this._winLevel();
    }
  }
  
  OnGhostEaten(ghost) {
    Res.SfxEatGhost.play();
    
    this.player.render = false;
    
    this._addScore(this.ghostPoints);
    let score = new Sprite(Res.Material, "score_" + this.ghostPoints);
    score.position = ghost.position;
    
    this.ghostPoints *= 2;
    
    this.IsPaused = true;
    this.timer = jGame.InvokeAfter(1, () => {
      score.Destroy();
      this.player.render = true;
      this.IsPaused = false;
      ghost.ReturnHome();
      Res.MusicGhostScared.stop();
      Res.MusicSiren.stop();
      if (!this.IsGhostReturningHome) {
        Res.MusicGhostReturnHome.play();
      }
      this.IsGhostReturningHome = true;
    });
  }
  
  OnPlayerEaten() {
    this.IsInGame = false;
    this.IsGameOver = true;
    this.AreGhostsScared = false;
    this.player.animationSpeed = 0;
    
    Res.MusicSiren.stop();
    Res.MusicGhostScared.stop();
    Res.MusicGhostReturnHome.stop();
    
    this.timer = jGame.InvokeAfter(1.5, () => {
      for (let ghost of this.ghosts) { ghost.Destroy(); }
      this.player.PlayDeathAnimation(() => this.OnDeathAnimationComplete());
      Res.SfxGameOver.play();
    });
  }
  
  OnDeathAnimationComplete() {
    this.timer = jGame.InvokeAfter(1.5, () => this._restartLevel(true));
  }
  
  _pause() {
    this.pausedText.render = true;
    this.IsUserPaused = true;
    this.player.animationSpeed = 0;
    this.WasPaused = this.IsPaused;
    this.IsPaused = true;
    this.timer.Pause();
    Res.MusicSiren.stop();
    Res.MusicGhostScared.stop();
  }
  
  _unpause() {
    this.pausedText.render = false;
    this.IsUserPaused = false;
    this.player.animationSpeed = 1;
    this.IsPaused = this.WasPaused;
    this.timer.Start();
    if (this.AreGhostsScared) {
      Res.MusicGhostScared.play();
    } else {
      Res.MusicSiren.play();
    }
  }
  
  Update() {
    // Toggle pause state if the player presses enter
    if (this.IsInGame) {
      if (Input.keyPressedThisFrame(Input.Key.ENTER)) {
        if (this.IsUserPaused) { this._unpause(); } else { this._pause(); }
      }
    }
    
    // Pause the big dots' flashing animation if we're in a cutscene.
    if (this.level) {
      for (let dot of this.level.bigDots) {
        if (this.IsPaused) {
          dot.animationTime = 0;
          dot.animationSpeed = 0;
        } else {
          dot.animationSpeed = 1;
        }
      }
    }
    
    // Flash the walls white 4 times if we beat the level.
    if (this.flashing && this.level) {
      this.flashTimer -= jGame.deltaTime;
      if (this.flashTimer <= 0) {
        this.flashing = false;
        for (let wall of this.level.walls) {
          wall.color = Color.black;
        }
        this.player.Destroy();
        this.timer = jGame.InvokeAfter(0.2, () => {
          this.level.Destroy();
          this.coffeeBreak = new CoffeeBreak();
          Res.MusicCoffeeBreak.play();
        });
      } else {
        for (let wall of this.level.walls) {
          wall.color = [Level.WallColor, Color.white][Math.floor(this.flashTimer/2*8)%2];
        }
      }
    }
    
    if (this.IsGhostReturningHome) {
      var returning = false;
      for (let ghost of this.ghosts) {
        if (ghost.returningHome) {
          returning = true;
          break;
        }
      }
      if (!returning) {
        this.IsGhostReturningHome = false;
        Res.MusicGhostReturnHome.stop();
        if (this.AreGhostsScared) {
          Res.MusicGhostScared.play();
        } else {
          Res.MusicSiren.play();
        }
      }
    }
    
    if (this.AreGhostsScared && !this.IsPaused) {
      this.scaredTimer -= jGame.deltaTime;
      var scared = 0;
      for (let ghost of this.ghosts) {
        if (ghost.scared) { ++scared; }
      }
      if (scared == 0 || this.scaredTimer <= 0) {
        this._unscareGhosts();
      }
    }
  }
}

let game = new PacmanGame();
