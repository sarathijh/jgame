class TitleScreen extends jGame.GameObject {
  constructor() {
    super();
    this.logo = new Sprite(Res.Material, {
      parent:   this,
      sprite:   "logo",
      position: new Vector2(jGame.canvas.width/2, 160 * Res.Scale),
      });
    
    this.startText = new FixedWidthText(Res.Material, "PRESS ENTER TO START");
    this.startText.parent = this;
    this.startText.Align("center", "bottom");
    this.startText.position = new Vector2(jGame.canvas.width/2, 100 * Res.Scale);
  }
  
  Update() {
    this.startText.render = jGame.Flash(jGame.time, 1);
    if (Input.keyPressedThisFrame(Input.Key.ENTER)) {
      if (this.delegate) {
        this.delegate.OnTitleSelect("START");
      }
    }
  }
}
