class Actor extends Sprite {
  constructor() {
    super(Res.Material, "poof");
    this.speed = 60 * Res.Scale;
    this.frameSpeed = 0;
    this.direction = null;
    this.nextTurn = null;
    this.hitWall = false;
  }
    
  /* override */ LateUpdate() {
    super.LateUpdate();
    this.hitWall = false;
  }
  
  TryTurn(turnDir, moveDir=null, lookAhead=0, onTurn=null) {
    if (turnDir && ((moveDir == null) || moveDir.NotEqual(turnDir))) {
      for (var i = 0; i <= lookAhead; ++i) {
        let offset = this.position.Plus(!moveDir ? Vector2.zero : moveDir.Times(i)).rounded;
        if (!Physics.BoxCast("wall", offset, this.extents, turnDir, lookAhead)) {
          if (onTurn) {
            onTurn(offset);
          }
          return offset;
        }
      }
    }
    return false;
  }
  
  Move() {
    this.frameSpeed = this.speed * jGame.deltaTime;
    
    // Try to turn in the nextTurn direction.
    this.TryTurn(this.nextTurn, this.direction, Math.ceil(this.frameSpeed), offset => {
      this.position = offset;
      this.direction = this.nextTurn;
    });
    
    // Move forward checking for wall collisions.
    if (this.direction) {
      Physics.BoxCast("wall", this.position.rounded, this.extents, this.direction, Math.ceil(this.frameSpeed), hit => {
        this.position.Round();
        this.hitWall = true;
        this.frameSpeed = hit.distance;
      });
      this.position.Add(this.direction.Times(this.frameSpeed));
    }
    
    // Wrap actor to opposite side of screen when out of bounds.
    this.position.Wrap(this.extents.Times(-0.5), jGame.canvas.size.Plus(this.extents.Times(0.5)));
  }
}
